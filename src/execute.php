<?php

$prog_text = filter_input(INPUT_POST, "code", FILTER_UNSAFE_RAW);

$file = "/tmp/" . md5($prog_text);

$write_ret = file_put_contents($file, $prog_text);

if ($write_ret === FALSE) {
  header("HTTP/1.1 500 Internal Server Error");
  exit;
}

$cmd = "/usr/bin/sbcl --script minihelpint.lisp " . $file ;

$output = shell_exec($cmd);

echo "0\n";
echo $output;

?>
