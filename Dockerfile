FROM php:8.2.1-apache-bullseye
WORKDIR /var/www/html

RUN apt-get update && apt-get upgrade
RUN apt-get install -y sbcl

COPY src/ .
COPY minihelpint.lisp .
COPY minihelp-functions.lisp .
EXPOSE 80
